module.exports = {
  'Home' : 'صفحه اصلی',
  'Request clinical services' : 'درخواست خدمات بالینی',
  'Membership' : 'عضویت',
  'forget password' : 'فراموشی رمز عبور ',
  'Password' : 'رمز عبور ',
  'Register' : 'ثبت نام ',
  'dir' : 'ltr',



  'The_name_field_is_required':'فیلد نام را وارد نمایید',
  'The_lastname_field_is_required':'فیلد نام خانوادگی را وارد نمایید',
  'The_address_field_is_required':'فیلد آدرس را وارد نمایید',
  'The_city_field_is_required':'فیلد استان را وارد نمایید',
  'The_state_field_is_required':'فیلد شهر را وارد نمایید',

  //Send Request
  'Service':'خدمات',
  'Type Service':'نوع سرویس',
  'Issue Tracking':'شماره پیگیری',
  'Upload':'آپلود',





  // Dashboard
  'Dashboard':'پیشخوان',
  'Add Service':'خدمات سرویس جدید',
  'Edit User':'ویرایش اطلاعات',


  // Edit User
  'General specifications':'مشخصات کلی',
  'Accompanying the client':'همراه مددجو',
  'Insurance':'بیمه',
  'disease background':'سابقه بیماری',
  'Sight and hearing':'بینایی و شنوایی',
  'The client movement position':'وضیعت حرکتی مددجو',
  'Nutrition':'تغذیه',
  'Male':'مرد',
  'Female':'زن',

  //Viewmedical
  'Send documents':'ارسال مدارک',
  'forwarded documents':'مدارک ارسال شده ',
  'Site documents':'مدارک سایت',


  'Request services List':'لیست درخواست خدمات',
  'Support':'پشتیبانی',
  'Laboratory and radiology results':'نتایج آزمایشگاه و رادیولوژی',
  'View medical records':'مشاهده مدارک پزشکی',
  'Your clinical condition':'وضیعت بالینی شما',
  'Transactions and accounting':'تراکنش ها و حسابداری',
  'Health And Update':'سلامتی و بروزرسانی',
  'Telemedicine':'تله مریسین',
  'Messages':'پیام ها',
  'Admission queue':'صف پذیرش',









  //Triage
  'Digital file':'پرونده دیجیتال',


  'waiting':'در حال انتظار',
  'doing':'در حال انجام',




















  'login':'ورود',
  'Enter your mobile number':'شماره موبایل خود را وارد نمایید',
  'example 09121234567':'مثال : 09121234567',
  'Identifier code':'کد معرف',
  'I read and accepted the rules.':' را مطالعه و قبول کردم.',
  'the rules':'قوانین',
  'Enter the 4-digit code sent':'کد 4 رقمی ارسال شده را وارد نمایید',
  'This service will be activated soon':'این سرویس بزودی فعال می شود.',
  'description':'توضیحات',
  'Rating':'نظر سنجی',
  'Confirmation':'تایید',
  'Male':'مرد',
  'Female':'زن',
  'Preview':'پیش نمایش',
  'Reset':'تنظیم مجدد',
  'Upload':'آپلود',
  'Close':'بستن',
  'Enter the code sent by the taxi driver':'کد ارسال شده از تاکسیرانی را وارد کنید',
  'Question from Oxin':'سوال از اکسین',
  'Write your message':'پیام خود را بنویسید',
  'Call':'تماس تلفنی',
  'Send mail':'ارسال پیام',
  'Save': 'ثبت',
  //  Index Dashboard

  'Account':'حساب کاربری',
  'vehicles':'وسایل نقلیه',
  'Exit':'خروج',
  'Credit':'اعتبار',
  'Service':'خدمات سرویس',
  'Taxi service':'خدمات تاکسی',
  'Request records':'سوابق درخواست',
  'Question from Auxin':'سوال از اکسین',
  'Support':'پشتیبانی',
  'Location':'مکان یابی',
  'Conditions':'شرایط',
  'Go to the site':'رفتن به سایت',
  'welfare Sevices':'خدمات رفاهی',
  'Complete the form':'تکمیل فرم ',
  'Request a letter of introduction':'درخواست معرفی نامه ',



  // Index User

  'Selected addresses':'آدرس های منتخب',
  'wallet':'کیف پول',




  'Your credit':'اعتبار شما',
  'Your score':'امتیاز شما',
  'Toman':'تومان',


  //  Support
  'About us':'درباره ما',
  'Frequently Asked Questions':'سوال متداول',
  'contact us':'تماس با ما',
  'Work with us':'همکاری با ما',
  'WebSite':'وب سایت',


  //
  'Locations':'آدرس های منتخب',

  //

  'News': 'خبرها',
  'Survey': 'نظر سنجی',
  'Technical tips': 'نکات فنی',
  'DRIVING TIPS': 'نکات رانندگی',

  // Shop
  'Shop': 'نکات رانندگی',
  'Order records': 'سوابق سفارش',
  'Product offer': 'پیشنهاد محصول',
  'View product list': 'مشاهده لیست محصولات',
  'More': 'بیشتر',
  'Amazing products': 'محصولات شگفت انگیز',
  'Latest Products': 'جدیدترین محصولات',
  'The best selling products': 'پرفروش ترین محصولات',

  //   ERORR
  'Enter the password.':'رمز عبور را وارد نمایید.',
  'The_password_must_be_at_least_8_characters':'رمز عبور باید پیش از 8 کارکتر باشد.',
  'The_phone_has_already_been_taken':'این شماره موبایل تکراری می باشد.',

  'The_title_field_is_required':'لطفا فیلد تیتر را پر نمایید',
  'The_description_field_is_required':'لطفا فیلد توضیحات را پر نمایید',
  'The_subject_field_is_required':'لطفا یکی از موضوعات را انتخاب نمایید',
  'The_address_field_is_required':'فیلد آدرس را وارد نمایید.',
  'The_city_field_is_required':'فیلد شهر را وارد نمایید.',
  'The_gender_field_is_required':'فیلد جنسیت را وارد نمایید.',
  'The_lastname_field_is_required':'فیلد جنسیت را وارد نمایید.',





  'The number entered is incorrect.':'شماره موبایل وارد شده صحیح نمی باشد',
  'You must accept the rules to register.':'برای ثبت نام باید قوانین را قبول کنید.',
  'The code field is required.':'کد وارد شده اشتباه می باشد.',
  'Enter the code':'کد را وارد نماید',
  'I realized':'متوجه شدم',
  'Enter the name field':'فیلد نام را وارد نمایید',
  'Enter the lastname field':'فیلد نام خانوادگی را وارد نمایید',
  'Enter the national code correctly':'کد ملی را درست وارد نمایید ',
  'Enter the submitted code.':'کد ارسالی را وارد نمایید. ',
  'The date field is required.':'قسمت تاریخ لازم است. ',
  'The time id field is required.':'قسمت شناسه زمان لازم است. ',
  'The address field is required.':'قسمت آدرس لازم است. ',
  'The name field is required':'قسمت نام الزامی است ',
  'Enter your message':'پیام خود را وارد نمایید ',
  'The code entered is incorrect .':'کد وارد شده صحیح نمی باشد ',
  'I am aware of the rules and regulations of taxi drivers and I confirm the information entered':'اینجانب ضمن آگاهی از قوانین و مقررات تاکسیرانی اطلاعات وارد شده را تایید می نمایم ',
  'You must confirm the rules to submit information.':'برای ارسال اطلاعات باید قوانین را تایید نمایید.',
  'New send code.':'ارسال کد جدید ',

}
