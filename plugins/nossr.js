import Vue from 'vue';

import VuePersianDatetimePicker from 'vue-persian-datetime-picker';

Vue.component('custom-date-picker', VuePersianDatetimePicker);


Vue.use(require('vue-jalali-moment'));




import FlipCountdown from 'vue2-flip-countdown';
Vue.component('FlipCountdown', FlipCountdown);


//  Google -----------------------
import { LMap, LTileLayer, LMarker, LIcon,LPopup } from 'vue2-leaflet';

Vue.component('l-map', LMap);
Vue.component('l-tile-layer', LTileLayer);
Vue.component('l-marker', LMarker);
Vue.component('LIcon', LIcon);
Vue.component('LPopup', LPopup);

import { Icon } from 'leaflet';
delete Icon.Default.prototype._getIconUrl;
Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});
