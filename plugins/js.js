import Vue from 'vue';
Vue.prototype.$url = 'https://api.macremedy24.com/api/';
Vue.prototype.$user = null;
Vue.prototype.$title = null;


import { JitsiMeet } from '@mycure/vue-jitsi-meet';
Vue.component('VueJitsiMeet', JitsiMeet);

import moment from 'jalali-moment';
Vue.use(moment);
Vue.prototype.$moment = moment ;
/*
//
Vue.use(VueMoment,require('jalali-moment'));
Vue.use(require('jalali-moment'), {
  moment
});
*/
import * as VueGoogleMaps from '~/node_modules/vue2-google-maps/src/main'
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyDX0ePlvqnGmKk3LfpJ3-ysZolbAIX8gVA',
    language:'fa',
    //libraries: 'places', // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
     libraries: 'places,drawing,visualization'
    // (as you require)
  }
});
