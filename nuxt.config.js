import colors from 'vuetify/es5/util/colors'

export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    titleTemplate: '%s - Macremedy24',
    title: 'Macremedy24',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},

      {rel: 'apple-touch-icon', type: 'image/x-icon', sizes: "57x57", href: `/icon/apple-icon-57x57.png`},
      {rel: 'apple-touch-icon', type: 'image/x-icon', sizes: "60x60", href: `/icon/apple-icon-60x60.png`},
      {rel: 'apple-touch-icon', type: 'image/x-icon', sizes: "72x72", href: `/icon/apple-icon-72x72.png`},
      {rel: 'apple-touch-icon', type: 'image/x-icon', sizes: "76x76", href: `/icon/apple-icon-76x76.png`},
      {rel: 'apple-touch-icon', type: 'image/x-icon', sizes: "114x114", href: `/icon/apple-icon-114x114.png`},
      {rel: 'apple-touch-icon', type: 'image/x-icon', sizes: "120x120", href: `/icon/apple-icon-120x120.png`},
      {rel: 'apple-touch-icon', type: 'image/x-icon', sizes: "144x144", href: `/icon/apple-icon-144x144.png`},
      {rel: 'apple-touch-icon', type: 'image/x-icon', sizes: "152x152", href: `/icon/apple-icon-152x152.png`},
      {rel: 'apple-touch-icon', type: 'image/x-icon', sizes: "180x180", href: `/icon/apple-icon-180x180.png`},

      {rel: 'icon', type: 'image/png', sizes: "192x192", href: `/icon/android-icon-192x192.png`},
      {rel: 'icon', type: 'image/png', sizes: "32x32", href: `/icon/favicon-32x32.png`},
      {rel: 'icon', type: 'image/png', sizes: "96x96", href: `/icon/favicon-96x96.png`},
      {rel: 'icon', type: 'image/png', sizes: "16x16", href: `/icon/favicon-16x16.png`},



    ],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    '~plugins/js.js',

    {
      src: '~plugins/nossr.js',
      ssr: false
    },
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    // https://go.nuxtjs.dev/content
    '@nuxt/content',

    'nuxt-leaflet',
    'nuxt-i18n',
    'nuxt-vue-select',
  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {},

  // Content module configuration (https://go.nuxtjs.dev/config-content)
  content: {},
  i18n: {
    locales: [
      {
        code: 'fa',
        file: 'fa-IR.js',
        flag: '/fa.png'
      },
      {
        code: 'en',
        file: 'en-US.js',
        flag: '/en.png'

      },

    ],
    defaultLocale: 'fa',
    lazy: true,
    langDir: 'lang/'

  },
  // Vuetify module configuration (https://go.nuxtjs.dev/config-vuetify)
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }

      },
      rtl:true
    }
  },
  server: {
    port: 2004, // default: 3000
    host: '0.0.0.0'
  },
  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
  }
}
